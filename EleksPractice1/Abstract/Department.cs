﻿using EleksPractice1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EleksPractice1.Abstract
{
    public abstract class Department : IComparable, ICountingBooks
    {
        private DateTime establishDate;
        private List<Book> books;

        public Department()
        {
            this.EstablishDate = DateTime.Now;
        }

        public Department(DateTime establishDate, List<Book> books)
        {
            this.Books = books;
            this.EstablishDate = establishDate;
        }

        public DateTime EstablishDate
        {
            get
            {
                return establishDate;
            }
            set
            {
                establishDate = value;
            }
        }


        public List<Book> Books
        {
            get
            {
                if (books == null)
                {
                    Books = new List<Book>();
                }
                return books;
            }
            set
            {
                books = value;
            }
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            Department otherDepartment = obj as Department;
            if (otherDepartment == null)
            {
                throw new ArgumentException(nameof(obj));
            }

            return Books.Count().CompareTo(otherDepartment.Books.Count());
        }

        public int BooksCount()
        {
            return Books.Count();
        }

        public void SortByName()
        {
            books.OrderBy(x => x.Name);
        }

        public override string ToString()
        {
            return $"This is department which was established at {this.EstablishDate.Date}";
        }
    }
}
