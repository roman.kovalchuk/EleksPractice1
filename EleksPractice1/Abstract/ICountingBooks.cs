﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EleksPractice1.Abstract
{
    public interface ICountingBooks
    {
        int BooksCount();
    }
}
