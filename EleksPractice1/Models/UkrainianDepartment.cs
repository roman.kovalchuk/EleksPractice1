﻿using EleksPractice1.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EleksPractice1.Models
{
    public class UkrainianDepartment : DepartmentBridge
    {
        private int nationalAcquisitionCount;

        public UkrainianDepartment(int nationalAacquisitionCount, DateTime establishDate, List<Book> books) : base(establishDate, books)
        {
            this.NationalAcquisitionCount = nationalAacquisitionCount;
        }

        public UkrainianDepartment() : base()
        {
            this.NationalAcquisitionCount = 0;
        }

        public int NationalAcquisitionCount
        {
            get
            {
                return nationalAcquisitionCount;
            }
            set
            {
                nationalAcquisitionCount = value;
            }
        }

        public override string ToString()
        {
            return $"This is Ukrainian DepartmentBridge with {NationalAcquisitionCount} national acquisitions";
        }
    }
}
