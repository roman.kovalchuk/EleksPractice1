﻿using EleksPractice1.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EleksPractice1.Models
{
    public class ForeignDepartment : DepartmentBridge
    {
        private List<string> languages;

        public ForeignDepartment(List<string> languages, DateTime establishDate, List<Book> books) : base(establishDate, books)
        {
            this.Languages = languages;
        }

        public ForeignDepartment() : base()
        {

        }

        public List<string> Languages
        {
            get
            {
                if (languages == null)
                {
                    Languages = new List<string>();
                }
                return languages;
            }
            set
            {
                languages = value;
            }
        }

        public override string ToString()
        {
            return $"This is Foreign DepartmentBridge with {Languages.Count} books with different languages";
        }
    }
}
