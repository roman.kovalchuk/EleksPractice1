﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EleksPractice1.Models
{
    public class Book : IComparable
    {
        private int pages;
        private Author author;
        private string name;

        public Book(string name, int pages)
        {
            Pages = pages;
            Name = name;
        }

        public Book() : this("***", 0)
        {

        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public int Pages
        {
            get
            {
                return pages;
            }
            set
            {
                pages = value;
            }
        }

        public Author Author
        {
            get
            {
                return author;
            }
            set
            {
                author = value;
            }
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            Book otherBook = obj as Book;
            if (otherBook == null)
            {
                throw new ArgumentException(nameof(obj));
            }

            return Pages.CompareTo(otherBook.Pages);
        }

        public override string ToString()
        {
            return $"This is \"{Name}\" book with {Pages} pages.";
        }

        
    }
}
