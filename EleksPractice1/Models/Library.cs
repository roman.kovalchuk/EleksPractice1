﻿using EleksPractice1.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EleksPractice1.Models
{
    public class Library : ICountingBooks
    {
        private string name;
        private List<DepartmentBridge> departments;

        public Library()
        {
            Name = "Unknown";
        }

        public Library(List<DepartmentBridge> departments, string name)
        {
            this.Departments = departments;
            this.Name = name;
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public List<DepartmentBridge> Departments
        {
            get
            {
                if (departments == null)
                {
                    Departments = new List<DepartmentBridge>();
                }
                return departments;
            }
            set
            {
                departments = value;
            }
        }

        public int BooksCount()
        {
            return Departments.Sum(x => x.BooksCount());
        }

        public Department DepartmentWithMaxBooks()
        {
            return Departments.Max();
        }

        public Book BookWithLeastPages()
        {
            return Departments.SelectMany(x => x.Books).Min();
        }

        public Author AuthorWithMaxBooksCount()
        {
            return Departments.SelectMany(x => x.Books).Select(x => x.Author).Max();
        }

        public override string ToString()
        {
            return $"This is \"{Name}\" library with {Departments.Count} departments.";
        }

    }
}
