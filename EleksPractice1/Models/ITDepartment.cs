﻿using EleksPractice1.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EleksPractice1.Models
{
    public class ITDepartment : DepartmentBridge
    {
        private int pcCount;

        public ITDepartment(int pcCount, DateTime establishDate, List<Book> books) : base(establishDate, books)
        {
            this.PcCount = pcCount;
        }

        public ITDepartment() : base()
        {
            this.PcCount = 0;
        }

        public int PcCount
        {
            get
            {
                return pcCount;
            }
            set
            {
                pcCount = value;
            }
        }

        public override string ToString()
        {
            return $"This is IT DepartmentBridge with {PcCount} PCs";
        }
    }
}
