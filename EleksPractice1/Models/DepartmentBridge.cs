﻿using EleksPractice1.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EleksPractice1.Models
{
    public class DepartmentBridge : Department
    {
        public DepartmentBridge() : base()
        {

        }

        public DepartmentBridge(DateTime establishDate, List<Book> books) : base(establishDate, books)
        {

        }
    }
}
