﻿using EleksPractice1.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EleksPractice1.Models
{
    public class Author : ICountingBooks, IComparable
    {
        private string firstName;
        private string lastName;
        private List<Book> books;

        public Author(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }
        public Author() : this("FirstName", "LastName")
        {

        }

        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
            }
        }

        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
            }
        }

        public List<Book> Books
        {
            get
            {
                return books;
            }
            set
            {
                books = value;
            }
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            Author otherAuthor = obj as Author;
            if (otherAuthor == null)
            {
                throw new ArgumentException(nameof(obj));
            }

            return Books.Count.CompareTo(otherAuthor.Books.Count);
        }

        public int BooksCount()
        {
            return Books.Count;
        }

        public override string ToString()
        {
            return $"This is {FirstName} {LastName}. He (she) is book writer.";
        }
    }
}
