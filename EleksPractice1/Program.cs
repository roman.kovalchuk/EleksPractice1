﻿using EleksPractice1.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EleksPractice1
{
    static class Program
    {
        static void Main()
        {
            //XMLService.Read();
            //XMLService.AddNewBook();
            //XMLService.SaveNew();
            //XMLService.Delete();
            //XMLService.Rename();
            JsonService.LibraryToJson();
            JsonService.JsonToLibrary();
            Console.ReadLine();
        }
    }
}
