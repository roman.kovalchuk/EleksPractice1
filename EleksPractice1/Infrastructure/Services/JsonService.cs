﻿using EleksPractice1.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace EleksPractice1.Infrastructure.Services
{
    public static class JsonService
    {
        private static Library library = new Library();

        static JsonService()
        {
            IniziallizateLibrary();
        }

        private static void IniziallizateLibrary()
        {
            library.Name = "NU LP";
            library.Departments.Add(new ITDepartment(10, DateTime.Now,
                new List<Book>
                {
                    new Book("C#",1000),
                    new Book("Java",800),
                    new Book("C++",850)
                }
                ));
            library.Departments.Add(new UkrainianDepartment(1, DateTime.Now,
                new List<Book>
                {
                    new Book("Kobzar",400),
                    new Book("Bukvar",100),
                    new Book("Fox",25)
                }
                ));
            library.Departments.Add(new ForeignDepartment(new List<string> { "English", "German", "French" },
                DateTime.Now, new List<Book>
                {
                    new Book("Harry Pother",3000),
                    new Book("Eragon",3800),
                    new Book("Narnia",1800)
                }
                ));

        }

        public static void LibraryToJson()
        {
            string json = JsonConvert.SerializeObject(library);
            File.WriteAllText(GetJsonPath(), json);
        }

        public static void JsonToLibrary()
        {
            using (StreamReader jsonFile = new StreamReader(GetJsonPath()))
            {
                string json = jsonFile.ReadToEnd();
                Library library = JsonConvert.DeserializeObject<Library>(json);
                PrintLibraryInfo(library);
            }
        }

        private static string GetJsonPath()
        {
            return Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "//library.json";
        }


        private static void PrintLibraryInfo(Library library)
        {
            Console.WriteLine($"Name: {library.Name}");
            foreach (DepartmentBridge DepartmentBridge in library.Departments)
            {
                Console.WriteLine(DepartmentBridge.ToString());
                Console.WriteLine($"Books: ");
                foreach (Book book in DepartmentBridge.Books)
                {
                    Console.WriteLine(book.ToString());
                }
            }
        }

    }
}
