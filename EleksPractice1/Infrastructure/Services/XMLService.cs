﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace EleksPractice1.Infrastructure.Services
{
    static class XMLService
    {
        public static void Read()
        {
            XDocument xml = XDocument.Load("Books.xml");
            if (xml == null || !xml.Root.HasElements)
            {
                return;
            }
            Console.WriteLine(xml.ToString());
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load("books.xml");
            XmlElement library = xDoc.DocumentElement;
            foreach (XmlNode xnode in library)
            {
                if (xnode.Attributes.Count > 0)
                {
                    XmlNode attr = xnode.Attributes.GetNamedItem("name");
                    if (attr != null)
                    {
                        Console.WriteLine(attr.Value);
                    }
                }
                foreach (XmlNode childnode in xnode.ChildNodes)
                {

                    if (childnode.Name == "books" && childnode.Attributes.Count > 0)
                    {
                        Console.WriteLine("Book: {0}", childnode.Attributes.GetNamedItem("name").Value);
                    }
                }
                Console.WriteLine();
            }

        }

        public static void SaveNew()
        {
            XDocument xdoc = new XDocument();
            XElement lenovo = new XElement("laptop");
            XAttribute lenovoNameAttr = new XAttribute("name", "ideapad");
            lenovo.Add(lenovoNameAttr);
            XElement dell = new XElement("laptop");
            XAttribute dellNameAttr = new XAttribute("name", "latitude");
            dell.Add(dellNameAttr);
            XElement laptops = new XElement("laptops");
            laptops.Add(dell);
            laptops.Add(lenovo);
            xdoc.Add(laptops);
            xdoc.Save("Laptops.xml");
        }

        public static void Delete()
        {
            XDocument xdoc = XDocument.Load("Laptops.xml");
            XElement root = xdoc.Element("laptops");

            foreach (XElement xe in root.Elements("laptop").ToList())
            {
                if (xe.Attribute("name").Value == "latitude")
                {
                    xe.Remove();
                }
            }

            xdoc.Save("Laptops2.xml");
        }

        public static void Rename()
        {
            XDocument xdoc = XDocument.Load("Laptops.xml");
            XElement root = xdoc.Element("laptops");

            foreach (XElement xe in root.Elements("laptop").ToList())
            {
                if (xe.Attribute("name").Value == "latitude")
                {
                    xe.Remove();
                    XElement dell = new XElement("laptop");
                    XAttribute dellNameAttr = new XAttribute("name", "inspiliron");
                    dell.Add(dellNameAttr);
                    root.Add(dell);
                }
            }

            xdoc.Save("Laptops3.xml");
        }
        public static void AddNewBook()
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load("Books.xml");
            XmlElement xRoot = xDoc.DocumentElement;
            XmlElement userElem = xDoc.CreateElement("books");
            XmlAttribute nameAttr = xDoc.CreateAttribute("name");
            XmlText nameText = xDoc.CreateTextNode("C#");
            nameAttr.AppendChild(nameText);
            userElem.Attributes.Append(nameAttr);
            xRoot.FirstChild.AppendChild(userElem);
            xDoc.Save("Books2.xml");
        }

    }
}
